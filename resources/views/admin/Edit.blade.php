@extends('admin.layouts.blank')

@section('main_container')

        <!-- page content -->
<div class="right_col" role="main">

    <div class="x_content">

        <div class="loading"></div>

        <form class="form-horizontal form-label-left" id="form" name="form" action="{{ action('Admin\ParticipantController@update', ['id' => $participant->id]) }}" method="POST" novalidate enctype="multipart/form-data">

            {!! csrf_field() !!}

            <p>Exemple Form Validation <code>parsleyJS</code></p>

            <span class="section">Information</span>

            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_surname">Name <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="name_surname" name="name_surname" class="form-control col-md-7 col-xs-12" placeholder="Exemple : Lorem Lorem" required="required" type="text" value="{{ $participant->name_surname }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" id="email" name="email" required="required" placeholder="EX : yourname@exemple.com" class="form-control col-md-7 col-xs-12" value="{{ $participant->email }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Téléphone <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="tel" id="phone" name="phone" required="required" data-validate-length="8" class="form-control col-md-7 col-xs-12" maxlength="8" value="{{ $participant->phone }}">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="media"></label>
                <div class="col-md-6 col-sm-6 col-xs-12">

                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img alt="image" src="{{ asset('app/upload/'.$participant->photo) }}">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" id="media" name="media">
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message">Message <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="message" required="required" name="message" class="form-control col-md-7 col-xs-12">{{ $participant->message }}</textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message">Status <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <select id="status" class="form-control" name="status" required="required">
                        <option value="Pending"  <?php if ($participant->status == 'Pending') echo 'selected'; ?> >Pending</option>
                        <option value="Accepted" <?php if ($participant->status == 'Accepted') echo 'selected'; ?>>Accepted</option>
                        <option value="Rejected" <?php if ($participant->status == 'Rejected') echo 'selected'; ?>>Rejected</option>
                    </select>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button type="reset" class="btn btn-primary">Cancel</button>
                    <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>

</div>
<!-- /page content -->
@push('scripts')

<script type="text/javascript">

    $('.loading').hide();

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
    });

    $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit){
            //this.submit();
            $('.loading').show();
            $.ajax({
                url: $('#form').attr('action'),
                method: 'POST',
                data: $('#form').serialize(),
                statusCode: {
                    500: function() {
                        $('.loading').hide();
                        return swal('Oups!', 'Une erreur s\'est produite, veuillez réessayer ultérieurement.', 'error');
                    }
                }
            }).done(function(response){
                $('.loading').hide();
                if(response.success) {
                    return swal("Success!", response.message, "success")
                }
                return swal('Oups!', response.message, 'error');
            });
        }
        return false;
    });
</script>

@endpush
@endsection


