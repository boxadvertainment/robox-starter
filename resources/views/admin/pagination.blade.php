@extends('admin.layouts.blank')

@push('stylesheets')

    <!--   Exemple to push style -->
    <style>
    </style>
@endpush

@section('main_container')

<!-- page content -->
    <div class="right_col" role="main">

        <div class="">
            <div class="col-sm-12">
                <h1>Liste Des  Participants</h1>
            </div>
            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Liste participants</h2>
                            <a class="btn btn-primary pull-right" href="{{ url('admin/form') }}">Add New Participant</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name & Surname</th>
                                    <th>Phone</th>
                                    <th>E-mail</th>
                                    <th>Messages</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($participants as $key => $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name_surname }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->message }}</td>
                                        <td>
                                            <a href="{{ action('Admin\ParticipantController@destroy', ['id' => $item->id]) }}" class="btn btn-danger">Delete</a>
                                            <a href="{{ action('Admin\ParticipantController@edit', ['id' => $item->id]) }}" class="btn btn-default">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    </div>
<!-- /page content -->
@push('scripts')

<script type="text/javascript">
    $(document).ready(function() {
        var handleDataTableButtons = function() {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function() {
            "use strict";
            return {
                init: function() {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

       // TableManageButtons.init();
    });
</script>

@endpush
@endsection


