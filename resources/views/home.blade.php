@extends('layout')

@push('stylesheets')
        <style>

                 html, body {
                         height: 100%;
                 }
                body {
                        margin: 0;
                        padding: 0;
                        width: 100%;
                        display: table;
                        font-weight: 100;
                }
                .main-wrapper {
                        text-align: center;
                        display: table-cell;
                        vertical-align: middle;
                }
                .content {
                        text-align: center;
                        display: inline-block;
                }
                .title {
                        font-size: 56px;
                }

        </style>
@endpush

@section('content')
        <div class="container">
                <div class="content">
                        <h1>Hello : {{ $user['name'] }}</h1>
                </div>
        </div>
@endsection