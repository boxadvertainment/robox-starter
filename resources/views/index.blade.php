@extends('layout')

@push('stylesheets')
        <style>

                 html, body {
                         height: 100%;
                 }
                body {
                        margin: 0;
                        padding: 0;
                        width: 100%;
                        display: table;
                        font-weight: 100;
                }
                .main-wrapper {
                        text-align: center;
                        display: table-cell;
                        vertical-align: middle;
                }
                .content {
                        text-align: center;
                        display: inline-block;
                }
                .title {
                        font-size: 56px;
                }

        </style>
@endpush

@section('content')
        <div class="container">
                <div class="content">
                        <div class="title"> You Are Welcome... </div>
                        <div class="btn-block text-center">
                                <p>Login Using Social Sites Exemple:</p>
                                <a class="btn btn-primary" href="{{ route('social.login', ['facebook']) }}">Facebook</a>
                                <p>Vous pouvez ajouter des autres connection via des réseaux sociaux comme ( Twitter, Google ...)</p>
                                {{--<a class="btn btn-primary" href="{{ route('social.login', ['twitter']) }}">Twitter</a>
                                <a class="btn btn-primary" href="{{ route('social.login', ['facebook']) }}">Facebook</a>
                                <a class="btn btn-primary" href="{{ route('social.login', ['google']) }}">Google</a>--}}
                        </div>
                </div>
        </div>
@endsection