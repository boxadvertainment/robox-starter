<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Participant;
use Carbon\Carbon;

class ParticipantController extends Controller{

    public function index(){
        return view('admin.pagination')->with([
            'participants' => Participant::all()
        ]);
    }

    public function store(Request $request){

        $validator = \Validator::make($request->all(), [
            'name_surname'  =>  'required',
            'email' =>  'required|email|unique:participants',
            'phone' =>  'required|regex:/^[0-9]{8}$/',
            'message'  =>  'required',
            //'website'  =>  'required|url',
            //'password'  =>  'required|between:6,15',
            //'heard'  =>  'required',
            //'hobbies'  =>  'required',
            //'number'  =>  'required|integer|between:2,6',
        ]);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);

        } else {
            $participant = new Participant();

            if ($request->hasFile('media')) {
                $media = $request->media;
                $fileExtention = $media->guessExtension();
                $mediaFieName = Carbon::now()->timestamp . '.' . $fileExtention;
                $media->move(public_path('app/upload/'),$mediaFieName);
                $participant->photo = $mediaFieName;
            }

            $participant->name_surname = $request->name_surname;
            $participant->email = $request->email;
            $participant->phone = $request->phone;
            $participant->message = $request->message;
            $participant->status = 'Pending';

            if ($participant->save()){
                return response()->json(['success' => true, 'message' => 'Ajout avec success.']);
            }
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
    }

    public function edit($id){
        $participant = Participant::find($id);
        return view('admin.Edit')->with([
            'participant' => $participant
        ]);
    }

    public function update(Request $request, $id){

        $participant = Participant::find($id);

        $validator = \Validator::make($request->all(), [
            'name_surname'  =>  'required',
            'email' =>  'required|email|unique:participants',
            'phone' =>  'required|regex:/^[0-9]{8}$/',
            'message'  =>  'required',
        ]);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);

        }else{

            if ($request->hasFile('media')) {
                \File::delete(public_path('app/upload/').$participant->photo);
                $media = $request->media;
                $fileExtention = $media->guessExtension();
                $mediaFieName = Carbon::now()->timestamp . '.' . $fileExtention;
                $media->move(public_path('app/upload/'),$mediaFieName);
                $participant->photo = $mediaFieName;
            }

            if ( $request->name_surname != $participant->name_surname){
                $participant->name_surname = $request->name_surname;
            }

            if ( $request->email != $participant->email){
                $participant->email = $request->email;
            }

            if ( $request->phone != $participant->phone){
                $participant->phone = $request->phone;
            }
            if ( $request->message != $participant->message){
                $participant->message = $request->message;
            }
            if ( $request->status != $participant->status){
                $participant->status = $request->status;
            }

            if ($participant->save()){
                return response()->json(['success' => true, 'message' => 'Modification fait avec success']);
            }
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
    }

    public function destroy($id){
        $participant = Participant::find($id);
        \File::delete(public_path('app/upload/').$participant->photo);
        $participant->delete();
        return view('admin.pagination')->with([
            'participants' => Participant::all()
        ]);
    }

}
