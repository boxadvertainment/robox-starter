<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard(){
        return view('admin.dashboard');
    }
    public function form(){
        return view('admin.create');
    }
    public function pagination(){
        return view('admin.pagination');
    }
    public function exemples(){
        return view('admin.exemples');
    }
}
