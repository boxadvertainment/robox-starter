<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Models\Social;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller{

    public function __construct(){
        $this->redirectPath = route('home');
    }

    public function redirectToProvider($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
        //notice we are not doing any validation, you should do it

        $social_user = Socialite::driver($provider)->user();

        //dd($user);
        // stroing data to our use table and logging them in
        $data = [
            'name'  =>  $social_user->getName(),
            'email' =>  $social_user->getEmail(),
            'providerID'    =>  $social_user->getId(),
            'avatar'    =>  $social_user->getAvatar(),
            'token' =>  $social_user->token,
        ];

        $user_db = Social::where('providerID', $social_user->getId())->first();

        //dd($user_db);

        if ($user_db <> null){

            return view('home')->with(['user' => $data]);

        }else{

            $social = new Social();
            //$user_social = Social::firstOrCreate($data);
            $social->save($data);
            return view('home')->with(['user' => $data]);

        }

        //Auth::login(Social::firstOrCreate($data));
        //after login redirecting to home page
        //return redirect($this->redirectPath()->with([ 'user' => $data ]));
    }
}
