<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'AppController@index');
Route::get('home', ['as' => 'home', 'uses' => function() {
    return view('home');
}]);
Route::get('social/login/redirect/{provider}', ['uses' => 'SocialAuthController@redirectToProvider', 'as' => 'social.login']);
Route::get('social/login/{provider}', 'SocialAuthController@handleProviderCallback');
// Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
// Route::post('signup', 'AppController@signup');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::auth();
    Route::get('/', 'AdminController@dashboard');
    Route::get('/form', 'AdminController@form');
    Route::get('/pagination', 'ParticipantController@index');
    Route::post('/store', 'ParticipantController@store');
    Route::get('/show/{id}', 'ParticipantController@edit');
    Route::post('/update/{id}', 'ParticipantController@update');
    Route::get('/delete/{id}', 'ParticipantController@destroy');
});