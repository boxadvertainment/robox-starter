var elixir = require('laravel-elixir');
require('./elixir-extensions');
require('./gulpfile-admin');

elixir(function(mix) {

    mix
        .sass('main.scss')
        .babel([
            'facebookUtils.js',
            'main.js'
        ], 'public/js/main.js')

        /*******************     scripts front End start here          ******************/
        .scripts([
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        ], 'public/js/vendor-front.js', 'bower_components')
        .scripts([
            // bower:js
            'bower_components/es6-promise/promise.js',
            'bower_components/sweetalert2/dist/sweetalert2.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            // endbower
        ], 'public/js/plugins-front.js', 'bower_components')
        /*******************     styles front End start here          ******************/
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css',
            'bower_components/sweetalert2/dist/sweetalert2.css',
            // endbower
        ], 'public/css/plugins-front.css', 'bower_components')
        .copy('bower_components/gentelella/vendors/bootstrap/fonts/', 'public/fonts')
        .copy('bower_components/gentelella/vendors/font-awesome/fonts/', 'public/fonts')
        .copy('bower_components/modernizer/modernizr.js', 'public/js')
        .images()
        .wiredep()
        .minifyCss()
        .compress()
        .browserSync({
            proxy: process.env.APP_URL
        });

    //.pluginsJS(['fastclick','validator','icheck','chartjs','parsley','sweetalert2','scrollbar'],'plugins-admin')
    //.pluginsJS(['bootstrap','gentelella'],'vendor-front')
    //.pluginsJS(['gentelella','datatable'],'vendor-admin')
    //.pluginsCSS(['sweetalert2'],'plugins-admin')
    //.pluginsCSS(['bootstrap','scrollbar','gentelella','fontawesome','datatable'],'vendor-admin')



});
