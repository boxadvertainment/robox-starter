<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration{
    public function up(){
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_surname')->nullable();
            $table->string('phone')->nullable();
            //$table->string('number')->nullable();
            $table->string('email')->unique();
            //$table->string('password');
            $table->string('status')->nullable();
            $table->string('photo')->nullable();
            //$table->string('website')->nullable();
            //$table->string('hobbies')->nullable();
            $table->text('message')->nullable();
            //$table->string('heard')->nullable();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::drop('participants');
    }
}
