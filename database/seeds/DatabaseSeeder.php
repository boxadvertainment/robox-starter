<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder{

    public function run(){
        $this->call('UsersTableSeeder');
    }
}
